#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#define BLOCKSIZE 512
/* 
	remember to ignore . and .. 
	now its a dag
		given that it is a dag ==> 
			remember to keep track of which inodes we have visited 

	remember to error check system calls

	remember to close file
*/

typedef struct Entry Entry;
size_t explore(char* path, Entry* list);
Entry* new_entry();
bool maybe_insert(ino_t inode_num, Entry* list);
void print_list(Entry* list);
void free_list(Entry* list);


int main(int argc, char* argv[]) {
	Entry* list = new_entry();
	
	printf("%lu\t%s\n", explore(argv[1], list), (argv[1] ? argv[1] : "."));
	
	free_list(list);

	return 0;
}


/* recursively calculate disk usage of path using list to 
ensure that hardlinks are only followed once */
size_t explore(char* path, Entry* list) {
	path = (path ? path : "."); 

	size_t return_val = 0;

	DIR *directory;
	struct dirent *fileEntry; 



	directory = opendir(path);
	if(directory == NULL) { /* if opendir has failed */
		fprintf(stderr, "du: ");  /*opendir failed */
		perror(path);
		exit(126);
		/*return(0);*/

	} else { /* if opendir succeeded and we now have a dirent  */
		errno = 0;
		while((fileEntry = readdir(directory)) != NULL) { /* while there are more files */
			if (errno) {
				fprintf(stderr, "du: readdir: ");
				perror(path);
				exit(126);
			}
			

			char *filename = fileEntry->d_name;
			ino_t inode_num = fileEntry->d_ino;

			struct stat stat_buf; /* allocate buffer for lstat */

			/* calculate new pathlength */
			size_t path_len = strlen(path);
			size_t name_len = strlen(filename);

			size_t new_len = path_len + name_len + 2; /* +1 for /, +1 for NULL */

			

			char* file_path = malloc(new_len); /*remember to error check this */
			


			strncpy(file_path, path, path_len+1);
			
			file_path = strncat(file_path, "/", new_len);
			file_path = strncat(file_path, filename, new_len);
			/*printf("(%s)\n", filename);*/
			


			if (lstat(file_path, &stat_buf)!=0) { 
				/* if lstat failed */
				fprintf(stderr, "du: ");
				perror(file_path);
				exit(126);

			} else { /* if lstat succeeded and we now have a stat_buf */
				

				bool is_dot = (strcmp(filename, ".")==0);
				/*printf("%d\n", is_dot);*/

				if ( strcmp(filename, "..")!=0 ) { 
					/* if this is not .. */



					/* check to see if we have already visited this and if not insert to list */
					if ( maybe_insert(inode_num, list) ) { /* this will be true if we have not explored inode_num yet */

						/*printf("\nFILEPATH: %s\n", file_path);*/

						return_val += stat_buf.st_blocks;  /* add the size of the file to the running total */


						if (S_ISDIR(stat_buf.st_mode) && !is_dot) {
							/* if this is a directory which is not (. or ..) */
							
							/* recursive call on each sub directory */
				
							size_t additional = explore(file_path, list);
							
							return_val += additional;

							printf("%lu \t%s\n", additional, file_path);


						} /* end if IS_DIR and !is_dot*/
						/* else return_val += stat_buf.st_blocks; if this is a file not a dir */

					} /* end if maybe_insert */

				} /* end if not (..) */

			} /* end if lstat worked */
			free(file_path);
		} /* end while */
		closedir(directory);
	} /* end if opendir worked */
	return return_val;
} /* end explore */




/* this will be an Set of inode_nums */
struct Entry {
	ino_t inode_num;
	Entry* next; /* the "next" Entry in the list */
};


/* recurseively free all elements in the list starting from the last one */
void free_list(Entry* list) {
	if (list->next) {
		free_list(list->next);
	} 
	free(list);
}


void print_list(Entry* list) {
	Entry* cur = list;
	while(cur) {
		printf("indoe_num: %llu, next: %p\n", cur->inode_num, cur->next);
		cur = cur->next;
	}
}

/* create a new Entry on the heap */
Entry* new_entry() {
	Entry* e = malloc(sizeof(Entry));
	if (!e) {
		fprintf(stderr, "du: ");
		perror("malloc");
		exit(126);
	}
	e->inode_num = 0;
	e->next = NULL;
	return e;
}

/* returns true if inode_num has been inserted, false if it was alread in the Set */
bool maybe_insert(ino_t inode_num, Entry* list) {
	Entry* cur = list;
	while(cur->next) {
		if (cur->inode_num == inode_num) return false;
		cur = cur->next;
	}

	if (cur->inode_num == inode_num) return false;
	cur->next = new_entry();
	cur->next->inode_num = inode_num;

	return true;
}








