#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char** argv) {
	/*printf("hello from %s\n", argv[0]);*/
	/*char str[1];*/

	/*while() {
		printf("\t%s: %s\n", argv[0], str);
	}*/

	/*char c = getc(stdin);*/
	char str[] = "X";
	char output[27];
	memset(&output, 0, 27);
	int i = 0;
	int read_stat = 1;
	while (read_stat > 0) {
		read_stat = read(0, &str, 1);
		if(read_stat > 0)
			output[i++] = str[0];
			/*printf("%s", str);*/
	}
	
	if (read_stat < 0) {
		perror("read");
		exit(EXIT_FAILURE);
	}
	if (read_stat == 0) {
		printf("%s: %s\n", argv[0], output);
		/*printf("end stream\n");*/
	}

	/*printf("goodbye from %s\n", argv[0]);*/

	return 0;
}