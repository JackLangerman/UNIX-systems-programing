#include <stdio.h>
#include <stdlib.h>

typedef struct mat Mat;
struct mat {
	int m, n;
	float** mat;
};

void initmat(Mat* mat, int m, int n);

void printmat(Mat* mat);

void eye(Mat* mat);
void freemat(Mat* mat);
void setall(Mat* mat, float value);
void matmul(Mat* result, Mat* A, Mat* B);
void set(Mat* mat, int i, int j, float value);


int main(int argc, char* argv[]) {
	printf("hello\n");

	// initalize a 3x5 matrix
	int m=3, n=5; /* m x n matrix -- rows = m, cols = n */
	
	Mat mat;
	initmat(&mat, m, n);

	// print the matrix
	printf("zeros:\n");
	printmat(&mat);
	printf("\n");

	// set all elements to 3.2
	setall(&mat, 3.2);

	// print again
	printf("3.2:\n");
	printmat(&mat);
	printf("\n");

	// set mat to the identity
	eye(&mat);

	// print again
	printf("identity:\n");
	printmat(&mat);
	printf("\n");

	// free 
	freemat(&mat);


	// new square matrix
	initmat(&mat, m, m);
	printf("new square\n");
	
	// print the matrix
	printf("zeros:\n");
	printmat(&mat);
	printf("\n");

	// set mat to the identity
	eye(&mat);

	// print again
	printf("identity:\n");
	printmat(&mat);
	printf("\n");

	// free 
	freemat(&mat);

	// now test matmul
	printf("\nmatmul:\n");

	m=5, n=2;
	int k=4;

	Mat A, B, C;
	initmat(&A, m, n);
	initmat(&B, n, k);
	initmat(&C, m, k);


	eye(&A);
	setall(&B, 3.2);
	setall(&C, 0);

	printmat(&A);
	printf("\n");
	printmat(&B);
	printf("\n");
	printmat(&C);
	printf("\n======\n");

	matmul(&C, &A, &B);

	printmat(&A);
	printf("•\n");
	printmat(&B);
	printf("=\n");
	printmat(&C);
	printf("\n");




	// now test matmul
	printf("\nmatmul:\n");

	m=5, n=2, k=4;

	eye(&A);
	setall(&B, 3.2);
	setall(&C, 0);

	set(&A, 2,0, -1);
	set(&A, 3,0, 0.5);
	set(&A, 3,1, 0.5);
	set(&A, 4,0, 2);
	set(&A, 4,1, 2);

	set(&B, 1, 1, 0.5);
	set(&B, 1, 2, 1.5);


	printmat(&A);
	printf("\n");
	printmat(&B);
	printf("\n");
	printmat(&C);
	printf("\n======\n");

	matmul(&C, &A, &B);

	printmat(&A);
	printf("•\n");
	printmat(&B);
	printf("=\n");
	printmat(&C);
	printf("\n");

	return 0;
}

void setall(Mat* mat, float value) {
	for(int i=0;i<mat->m;i++) 
		for(int j=0;j<mat->n;j++) 
			mat->mat[i][j] = value;
}

void initmat(Mat* mat, int m, int n) {
	// Mat mat = *_mat;
	mat->m = m;
	mat->n = n;
	mat->mat = (float**) calloc(m, sizeof(float*));
	for(int i=0;i<m;i++)
		mat->mat[i] = (float*) calloc(n, sizeof(float));
}

void eye(Mat* mat) {
	for(int i=0;i<mat->m;i++) 
		for(int j=0;j<mat->n;j++) 
			mat->mat[i][j] = ( i==j ? 1 : 0);
}

void printmat(Mat* mat) {
	for(int i=0;i<mat->m;i++) {
		for(int j=0;j<mat->n;j++) {
			printf("%.2f  ", mat->mat[i][j]);
		}
		printf("\n");
		fflush(stdout);
	}
}

void freemat(Mat* mat) {
	for(int i=0;i<mat->m;i++)
		free(mat->mat[i]);
	free(mat->mat);
}

void set(Mat* mat, int i, int j, float value) {
	mat->mat[i][j] = value;
}

void matmul(Mat* result, Mat* A, Mat* B) {
	// assert(A->n == B->m);
	for(int i=0;i<A->m;i++) 
		for (int k=0;k<A->n;k++) 
			for (int j=0;j<B->n;j++)
				result->mat[i][j] += A->mat[i][k] * B->mat[k][j];
}




