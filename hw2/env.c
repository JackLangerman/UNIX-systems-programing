#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define OK 0
#define MALLOC_ERR 17
#define EXEC_ERR 21
#define PRINT_ERR 42

#define true 1
#define false 0
#define bool _Bool

extern char** environ;

void printArray(char** ary);

int main(int argc, char* argv[]) {
	bool need_to_free = false; /* var to keep track of if we malloc up a new env */
	
	if (argc > 1) { /* if there is at least one argument */

		bool using_i = (strcmp(argv[1], "-i") == 0);  /* if the -i option is used */

		/* count the key=value pairs */
		unsigned int arg_offset = (using_i ? 2 : 1);
		unsigned int i = arg_offset; /* i is the index into argv, used throughout rest of program */
		while (argv[i] && strstr(argv[i], "=")) i++; /* while argv[i] contains '=' and !NULL*/
		
		unsigned int var_count = i - arg_offset;    

		/* if there was atleast 1 key=value pair or we are using -i 
			i.e. if we need to modify the contents of environ */
		if (using_i || var_count > 0) { 

			unsigned int prev_length = 0; /* variable to hold length of old env */

			while(!using_i && environ[++prev_length]) /* get lenth of old env */; 


			unsigned int new_length = prev_length + var_count + 1;  /* num entries of the new env */


			char** new_env = malloc(new_length * sizeof(char*));
			
			if (!new_env) { /* if malloc has failed */
				perror("env");
				exit(MALLOC_ERR);
			}

			need_to_free = true;

			/* copy over the old env into new_env*/
			memcpy(&new_env[0], environ, (prev_length) * sizeof(char*)); 

			/* and then copy the new key=value pairs to new_env as well */
			memcpy(&new_env[prev_length], &argv[arg_offset], var_count * sizeof(char*));

			new_env[new_length-1] = NULL; /* don't forget to null terminate */

			environ = new_env;   /* point environ to the new environment */

		} /* end if atleast 1 key=value pair */
		
		/* execute the program in the new environment */
		if (argc > i) {
			char* cmd = argv[i];
			char** args = &argv[i];

			execvp(cmd, args);
			/* if all goes well the program will not execute anything 
			   after this point because execvp will never return. */

			char* err = strerror(errno);
			if (fprintf(stderr, "env: %s: %s\n", cmd, err) < 0) exit(PRINT_ERR);

			exit(EXEC_ERR);
		} /* end if we are execing */

	} /* end if at least 2 in argv */

	printArray(environ);

	if(need_to_free) free(environ); /* should really need to do this but just to be sure */

	return OK;
}

void printArray(char** ary) {
	while(*ary) {
		printf("%s\n", *ary);
		ary++;
	}
}


