#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>

#define MAXLINE     4096
#define PORT 2345
#define	LISTENQ		1024
#define BUFFSIZE    8192 


int main(int argc, char* argv[]) {

	printf("hello\n");

	int    listen_fd, connfd; 
	struct sockaddr_in servaddr;
    char   buff[MAXLINE];


	if((listen_fd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
		perror("socket");
        exit(EXIT_FAILURE);
	}

	memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;           	// Specify the family
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 	// use any available
    servaddr.sin_port        = htons(PORT);	  		// port to listen on


    /* set sock opt ? */

	if(bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
		perror("bind");
		exit(EXIT_FAILURE);
	}

	if(listen(listen_fd, LISTENQ) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

	for ( ; ; ) {
        
        struct fd_set* inputs;
        FD_ZERO(&inputs);

        FD_SET()



        printf("about to accept and block on server\n");

        // 5. Block until someone connects.
        //    We could provide a sockaddr if we wanted to know details of whom
        //    we are talking to.
        //    Last arg is where to put the size of the sockaddr if
        //    we asked for one
        // connfd = accept(listen_fd, (struct sockaddr *) NULL, NULL);
        if((connfd = accept(listen_fd, NULL, NULL)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        // We had a connection.  Do whatever our task is.

        printf("connection happened\n");

        snprintf(buff, sizeof(buff), "hello from server\n");
        int len = strlen(buff);
        if (len != write(connfd, buff, len)) {
            perror("write to connection failed");
        }

        memset(&buff, 0, len);
        if(read(connfd, buff, len) < 0) {
            perror("read");
            close(connfd);
            continue;
        }

        printf("read buf: %s", buff);

        len = strlen(buff);

        // 6. Close the connection with the current client and go back
        //    for another.
        printf("close fd and go aroujnd again\n");
        close(connfd);
    }

    printf("closing\n");

    close(listen_fd);

	return 0;

} /* end main */

