#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>

#define	LISTENQ		1024

int getPort(int argc, char* argv[]);
int setup_listen(int port);


// linked list of clients -- needs a lock around it
pthread_mutex_t clientListLock = PTHREAD_MUTEX_INITIALIZER;
typedef struct clientNode {
	int fd; // socket that we are keeping for this client
	struct clientNode* next;
};
typedef struct clientNode ClientNode;



// linked list of messages -- needs a lock around it
pthread_mutex_t messageListLock = PTHREAD_MUTEX_INITIALIZER;
typedef struct messageNode {
	struct messageNode* next;
	char* text;
};
typedef struct messageNode MessageNode;



typedef struct argStruct {
	MessageNode message_head;
	ClientNode client_head;
};
typedef struct argStruct ArgStruct;



int main(int argc, char* argv[]) {

	int port = getPort(argc, argv);
	printf("listening for connections on port %d.\n", port);
	

	
	int listen_fd = setup_listen(port);
	// listen for clients



	// make client list (threadsafe)
	ClientNode client_head;
	client_head.next = NULL;


	// make message queue  (theadsafe)
	MessageNode message_head;
	message_head.next = NULL;


	/* spawn "consumer" -- this thread will take messages off the queue
		and broadcast them to all clients	*/

	/* spawn "accepter" -- this thread will accept connections from 
		clients who connect via socket above. Will repedly call accept(listen_fd)
		as long as there is a client, after accepting the connection it will add
		the client to the client list and spawn a new thread to manange the task
		of adding that clinet's messages to the message queue */

	/* join */ 

	return 0;
}

void* consume(void* arg) {
	// this needs the queue of messages and the queue of clients
	ArgStruct* Qs = (ArgStruct*) arg;

	MessageNode message_head = Qs->message_head;
	ClientNode client_head = Qs->client_head;

	pthead_mutex_lock(&messageListLock);
	printf("got the lock\n");
	pthead_mutex_unlock(&messageListLock);
	printf("let it go\n");

	return NULL;
}

int getPort(int argc, char* argv[]) {
	int port = -1;
	if(argc > 1) {
		char* end;
        port = strtol(argv[1], &end, 10);
        if (*end) {
            fprintf(stderr, "can't convert %s to int.\n", end);
        }
	}
	if(port<0) {
		fprintf(stderr, "bad port specification");
	}
	return port;
}



int setup_listen(int port) {
	int listen_fd = -1;

	// setup a socket  to listen for new clients 
    if((listen_fd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }


    // setup struct for server address and port (with marshaling)
    struct sockaddr_in servaddr;

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;             // Specify the family
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   // use any available
    servaddr.sin_port        = htons(port);         // port to listen on
	


	// accociate an fd with a server addr and port
	if(bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
        perror("bind");
        exit(EXIT_FAILURE);
    }


    //put socket into state where it can accept connections
    if(listen(listen_fd, LISTENQ) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    return listen_fd;
}

