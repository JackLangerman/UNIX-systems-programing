#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>


/* linked list of messages */
typedef struct message Message;
struct message {
	char c;
	Message* next;
};

Message* init_message(Message* m) {
	m->c = ':';
	m->next = NULL;
}

void print_list_helper(Message* first) {
	printf("%c", first->c);
	if(first->next) print_list_helper(first->next);
}
void print_list(Message* first) {
	print_list_helper(first);
	printf("\n");
}

void free_list(Message* first) {
	if(first->next) free_list(first->next);
	free(first);
}

typedef struct head_with_lock HeadWithLock;
struct head_with_lock {
	Message** first;
	pthread_mutex_t* lock;
};

/* end linked list of messages */


/* producer */
void* produce(void* args) {
	// printf("producer: ");
	HeadWithLock* hwl = (HeadWithLock*) args;
	Message* next = *(hwl->first);
	pthread_mutex_t lock = *(hwl->lock);
	// Message* next;

	for (char letter = 'a'; letter <= 'z'; letter++) {
		printf("produce %c\n", letter);
		next->next = malloc(sizeof(Message));
		next = next->next;
		init_message(next);
		next->c = letter;
		sleep(1);
	}
	// printf("\n");
	printf("done producing\n");
	pthread_exit(0);
	return NULL;
}
/* end producer */

void* consume(void* args) {
	// Message* next = (Message*) args;
	printf("\t\t\t\tstart consumer %d\n", pthread_self());
	fflush(stdout);
	HeadWithLock* hwl = (HeadWithLock*) args;
	pthread_mutex_t* lock = hwl->lock;

	while(true) {
		pthread_mutex_lock(lock);
		Message* head = *(hwl->first);
		
		if(head->next == NULL) {
			pthread_mutex_unlock(lock);
			break;
		}

		Message* old = head;
		*(hwl->first) = head->next;
		pthread_mutex_unlock(lock);

		sleep(5);
		printf("\t\t\t\t%d: %c\n", pthread_self(), old->next->c);
		fflush(stdout);
		free(old);
	}
	printf("\t\t\t\tend consumer %d\n", pthread_self());
	fflush(stdout);
}


int main(int argc, char* argv[]) {
	Message* first = malloc(sizeof(Message));

	pthread_mutex_t list_lock = PTHREAD_MUTEX_INITIALIZER;

	HeadWithLock hwl;
	hwl.first = &first;
	hwl.lock = &list_lock;

	// produce(&hwl);
	pthread_t prod_t;
	pthread_create(&prod_t, NULL, produce, &hwl);


	int num_threads = 12;
	pthread_t consumers[num_threads];

	for (int i=0;i<num_threads;i++) {
		pthread_create(&consumers[i], NULL, consume, &hwl);
		sleep(1);
	}
	


	for (int i=0;i<num_threads;i++) 
		pthread_join(consumers[i], NULL);

	exit(0);
}









