#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#define VERBOSE false

/* linked list of messages */
typedef struct message Message;
struct message {
	char c;
	Message* next;
};

void init_message(Message* m) {
	m->c = ':';
	m->next = NULL;
}

void print_list_helper(Message* first) {
	printf("%c", first->c);
	if(first->next) print_list_helper(first->next);
}
void print_list(Message* first) {
	print_list_helper(first);
	printf("\n");
}

void free_list(Message* first) {
	if(first->next) free_list(first->next);
	free(first);
}

typedef struct list List;
struct list {
	Message** first;
	Message** last;
	pthread_mutex_t* lock;
	pthread_cond_t* not_empty;
	bool done;
};

/* end linked list of messages */


/* producer */
void* produce(void* args) {
	printf("start producing\n");

	List hwl = *((List*) args); // get a copy of args as List

	
	pthread_mutex_t* lock = hwl.lock; // address of the real lock
	

	for (char letter = 'a'; letter <= 'z'; letter++) {
		

		// allocate space for new message and prepare it
		Message* new_message = malloc(sizeof(Message)); 
		if(new_message == NULL) {
			char err[] = "on _ malloc:";
			sprintf(err, "on %c malloc:", letter);
			perror(err);
		}
		
		init_message(new_message);
		new_message->c = letter;

		sleep(0); // sleep to simulate working on stuff


		pthread_mutex_lock(lock); // aquire the lock
		
		(*(hwl.last))->next = new_message;   // save the new message as next
		hwl.last = &((*(hwl.last))->next);   // bump up tail pointer to tail->next
		
		printf("produce %c\n", letter);
		fflush(stdout);

		pthread_cond_broadcast(hwl.not_empty);

		pthread_mutex_unlock(lock); // release the lock
	
	} // done producing (end for)



	pthread_mutex_lock(lock); 		// aquire the lock

	((List*) args)->done = true;	// tell everyone producion has concluded
	pthread_cond_broadcast(hwl.not_empty);

	pthread_mutex_unlock(lock); 	// release the lock


	printf("done producing\n");
	fflush(stdout);

	return NULL;
}
/* end producer */


/* consumer */
void* consume(void* args) {
	printf("\t\t\t\tstart consumer %lu\n", (unsigned long) pthread_self());
	fflush(stdout);
	List* hwl = (List*) args;				// get pointer to List
	pthread_mutex_t* lock = hwl->lock;		// get pointer to lock

	bool done = false;
	if(VERBOSE) printf("(about to consumer loop %lu )\n", (unsigned long) pthread_self());
	Message* head = *(hwl->first);
	while( !done ) {
		if(VERBOSE) printf("(about to aquire lock %lu )\n", (unsigned long) pthread_self());
		if( (head = *(hwl->first)   )->next == NULL   &&    hwl->done) break;

		pthread_mutex_lock(lock);			// aquire lock
		// Message* head;// = *(hwl->first);		// get pointer to head

		if(VERBOSE) printf("(about to check next not NULL %lu )\n", (unsigned long) pthread_self());
		while(   (   head = *(hwl->first)   )->next == NULL ) {
			if(VERBOSE) printf("(top of while %lu )\n", (unsigned long) pthread_self());
			// done = hwl->done;
			if ( (done = hwl->done) ) {
				fprintf(stderr, "\t\t%lu DONE, head=%c, next=%p.\n", 
							(unsigned long) pthread_self(), 
							head->c,
							head->next);
				if (head->next == NULL) {
					pthread_cond_broadcast(hwl->not_empty);
					break;
				}
			}
			if(VERBOSE) printf("(about to wait %lu )\n", (unsigned long) pthread_self());
			fflush(stdout);
			pthread_cond_wait(hwl->not_empty, lock);
			if(VERBOSE) printf("(woke up %lu )\n", (unsigned long) pthread_self());
			fflush(stdout);
		}
		if(VERBOSE) printf("(just broke out of while, checking done %lu )\n", (unsigned long) pthread_self());
		// if( (done = hwl->done) ) break;
		if( (head = *(hwl->first)   )->next == NULL   &&    hwl->done) break;



		if(VERBOSE) printf("(about to actually consume stuff %lu )\n", (unsigned long) pthread_self());
		// if we are here we have something to consume

		Message* old = head;			// save a pointer to head (what we will consume)
		*(hwl->first) = head->next;		// bump up head pointer past the thing we will consume

		if(VERBOSE) printf("(about to release the lock %lu )\n", (unsigned long) pthread_self());
		pthread_mutex_unlock(lock);		// release the lock

		if(VERBOSE) printf("(sleep for a bit %lu )\n", (unsigned long) pthread_self());
		sleep(3);						// sleep to simulate doing some work
		if(VERBOSE) printf("(woke up %lu )\n", (unsigned long) pthread_self());

		printf("\t\t\t\t%lu: %c\n", (unsigned long) pthread_self(), old->next->c); // print results
		fflush(stdout);

		if(VERBOSE) printf("(freeing %lu )\n", (unsigned long) pthread_self());
		free(old);						// free the thing now that we have consumed it
		if(VERBOSE) printf("(freed %lu )\n", (unsigned long) pthread_self());

		// printf("\n\n==============\n\n\t\t\t%d\n\n==============\n\n", hwl->done);
		// done = hwl->done;
		// return NULL;
		if(VERBOSE) printf("(tell everyone else to wakeup %lu )\n", (unsigned long) pthread_self());
		pthread_cond_broadcast(hwl->not_empty);
		// done = hwl->done;
		if(VERBOSE) printf("(done telling everyone, bottom of loop...done=%d.\t %lu )\n", 
			// done=hwl->done,
			done,
			(unsigned long) pthread_self());
	} // end consumption loop

	printf("\t\t\t\tend consumer %lu\n", (unsigned long) pthread_self());
	fflush(stdout);
	return NULL;
}
/* end consumer */


int main(int argc, char* argv[]) {
	// setup / initalize the linked list with a dummy head
	Message* HEAD = malloc(sizeof(Message));
	if(HEAD == NULL) {
		perror("malloc:");
		exit(EXIT_FAILURE);
	}
	init_message(HEAD);

	pthread_mutex_t list_lock = PTHREAD_MUTEX_INITIALIZER;
	pthread_cond_t not_empty = PTHREAD_COND_INITIALIZER;

	List hwl;
	hwl.first = &HEAD;
	hwl.last = &HEAD;
	hwl.lock = &list_lock;
	hwl.done = false;
	hwl.not_empty = &not_empty;


	// done setting up list, start the producer thread
	
	printf("List is setup. About to produce.\n");
	pthread_t prod_t;
	pthread_create(&prod_t, NULL, produce, &hwl);


	// production thread should be running, about to start up consumers

	printf("Producer running. About to consume.\n");
	

	sleep(5);  // sleep to let producer get a few things in the queue


	//start up num_threads consumers
	int num_threads = 10;
	pthread_t consumers[num_threads];

	for (int i=0;i<num_threads;i++) {
		pthread_create(&consumers[i], NULL, consume, &hwl);
		sleep(0);
	}

	// join the consumers
	for (int i=0;i<num_threads;i++) 
		pthread_join(consumers[i], NULL);

	// join the producer 
	pthread_join(prod_t, NULL);

	// exit(0);
	return 0;
}









