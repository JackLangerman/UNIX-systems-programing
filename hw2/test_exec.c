#include <stdio.h>

extern char** environ;

int main(int argc, char* argv[]) {
	fprintf(stdout, "argc: %d argv: ", argc);
	char** p = argv;
	while(*p)
		fprintf(stdout, "%s ", *p++);

	fprintf(stdout, "\n");
	
	p = environ;
	while(*p)
		fprintf(stdout, "%s\n", *p++);

	fprintf(stdout, "\n");

	return 0;
}