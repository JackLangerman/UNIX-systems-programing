#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <stdbool.h>
#include <fcntl.h>

#define MAXLINE     4096
#define PORT        2345
#define	LISTENQ		1024
#define BUFFSIZE    8192 

#define TIMEOUT_SECS 10



bool set_block(int fd, bool blocking);



int main(int argc, char* argv[]) {
	
    bool is_server = true;

    char   buff[MAXLINE];


    int peer_fd; 
    int listen_fd;

    // setup a socket 
    if((listen_fd = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket");
        exit(EXIT_FAILURE);
    }


    // setup struct for server address and port (with marshaling)
    struct sockaddr_in servaddr;

    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;             // Specify the family
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);   // use any available
    servaddr.sin_port        = htons(PORT);         // port to listen on





    if (is_server) {
        // server stuff

    	if(bind(listen_fd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    		perror("bind");
    		exit(EXIT_FAILURE);
    	}



    	if(listen(listen_fd, LISTENQ) < 0) {
            perror("listen");
            exit(EXIT_FAILURE);
        }


        
        if((peer_fd = accept(listen_fd, NULL, NULL)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }

        // connection happened

        printf("connection happened\n");
        snprintf(buff, sizeof(buff), "hello from server\n");
        int len = strlen(buff);
        if (len != write(peer_fd, buff, len)) {
            perror("write to connection failed");
        }
        memset(buff, 0, len);

    }    //end server stuff

    else { // if we are the client
        
        //Connect to remote server
        if (connect(sock, (struct sockaddr *)&servaddr , sizeof(servaddr)) < 0) {
            perror("connect failed. Error");
            exit(EXIT_FAILURE);
        }
    }

    // peer_fd should now be set
    


    struct timeval timeout;

    // setup fd set for select

    struct fd_set inputs;
    // int max_fd = (listen_fd > peer_fd ? listen_fd : peer_fd) + 1;
    int max_fd = peer_fd+1;

    timeout.tv_sec = TIMEOUT_SECS;
    timeout.tv_usec = 0;



    // set both the socket and stdin to non-blocking
    set_block(STDIN_FILENO, false);
    set_block(peer_fd, false);


	for ( ; ; ) {   
        // printf("loopin");
        // snprintf(buff, sizeof(buff), "hello from (the loop in) the server\n");
        // len = strlen(buff);
        // if (len != write(peer_fd, buff, len)) {
            // perror("write to connection failed");
        // }
        memset(buff, 0, len); 
        

        // configure fd set
        FD_ZERO(&inputs);

        FD_SET(STDIN_FILENO, &inputs);
        // FD_SET(listen_fd, &inputs);
        FD_SET(peer_fd, &inputs);

      
        timeout.tv_sec = TIMEOUT_SECS;
        timeout.tv_usec = 0;



        printf("\t->stdin %d, sock %d\n", FD_ISSET(STDIN_FILENO, &inputs)!=0, FD_ISSET(listen_fd, &inputs)!=0);
       

        int num_fds = select(max_fd, &inputs, NULL, NULL, &timeout);
       

        printf("\t->stdin %d, sock %d\n", FD_ISSET(STDIN_FILENO, &inputs)!=0, FD_ISSET(listen_fd, &inputs)!=0);
        // FD_ZERO(&inputs);
        printf("\t->stdin %d, sock %d\n\n", FD_ISSET(STDIN_FILENO, &inputs)!=0, FD_ISSET(listen_fd, &inputs)!=0);




        // if peer is available for reading

        if(FD_ISSET(peer_fd, &inputs)) {
            printf("=============\npeer_fd(isset: %d)\n", FD_ISSET(peer_fd, &inputs)!=0);

            while( read(peer_fd, buff, BUFFSIZE) > 0 ) {
                int len = strlen(buff);
                if (len != write(STDOUT_FILENO, buff, len)) {
                    perror("write to stdout failed");
                }
                if (len != write(peer_fd, buff, len)) {
                    perror("write to connection failed");
                }
                printf("%s\n", buff);
                memset(buff, 0, len);
            }

            // FD_CLR(listen_fd, &inputs);
            printf("listen_fd(isset: %d)\n===============\n\n", FD_ISSET(listen_fd, &inputs)!=0);
            sleep(1);
        }


        // if stdin is available for reading

        if(FD_ISSET(STDIN_FILENO, &inputs)) {
            printf("=============\nstdin_fd(isset: %d)\n", FD_ISSET(STDIN_FILENO, &inputs)!=0);
            int c;
            while((c = read(STDIN_FILENO, buff, BUFFSIZE)) > 0 ) {
                printf("read %d bytes\n", c);
                int len = strlen(buff);
                if (len != write(STDOUT_FILENO, buff, len)) {
                    perror("write to stdout failed");
                }
                if (len != write(peer_fd, buff, len)) {
                    perror("write to connection failed");
                }
                printf("buff after read -> %s\n", buff);
                memset(buff, 0, len);
            }
            // FD_CLR(STDIN_FILENO, &inputs);
            printf("stdin_fd(isset: %d)\n===============\n\n", FD_ISSET(STDIN_FILENO, &inputs)!=0);
            sleep(1);
        }




        printf("\tnowzero?->stdin %d, sock %d\n\n", FD_ISSET(STDIN_FILENO, &inputs)!=0, FD_ISSET(listen_fd, &inputs)!=0);

        memset(buff, 0, len);
        



        
        if (num_fds == 0) {
            fprintf(stderr, "timeout...\n");
            exit(EXIT_FAILURE);
        } else if (num_fds < 0) {
            perror("select");
            exit(EXIT_FAILURE);
        }

        

 
    }

    close(peer_fd);  
    
    printf("closing\n");

    close(listen_fd);

	return 0;

} /* end main */





bool set_block(int fd, bool blocking) /* set sock opt ? */ {
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1) return false;
    flags = blocking ? (flags & ~O_NONBLOCK) : (flags | O_NONBLOCK);
    return (fcntl(fd, F_SETFL, flags) == 0) ? true : false;
}
