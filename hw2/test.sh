make test

printf "\nEach test will print differences between "\
"env and env. If there's nothing there then I couldn't find any problems.\n\n"

#test no args and no env vars
printf "test with no args but with env vars\n"
env -i env      &> m1
env -i ./env &> m2
diff m1 m2

#test with no args but with env vars 
printf "test with no args but with env vars\n"
env -i foo=bar env      &> m1
env -i foo=bar ./env &> m2
diff m1 m2

#test with outer env vars and inner env vars
printf "test with outer env vars and inner env vars\n"
env -i foo=bar env foo2=bar2       &> m1
env -i foo=bar ./env foo2=bar2  &> m2
diff m1 m2

#test exec 
printf "test exec\n"
env -i foo=bar ./test_exec these are my args      &> m1
./env -i foo=bar ./test_exec these are my args &> m2
diff m1 m2

env -i foo=bar env infoo=inbar ./test_exec these are the args      &> m1
env -i foo=bar ./env infoo=inbar ./test_exec these are the args &> m2
diff m1 m2

#test -i without vars
printf "test -i without vars\n"
env -i env -i                 &> m1  
env -i ./env -i            &> m2      
diff m1 m2

#test -i with vars
printf "test -i with vars\n"
env -i foo=bar env  -i        &> m1        
env -i foo=bar ./env -i    &> m2         
diff m1 m2

printf "test trying to exec a file that isn't there\n"
env -i foo=bar blah 		&> m1  
./env -i foo=bar blah 	&> m2 
diff m1 m2

rm m1 m2



printf "\n\n===========================\n\nnow going to valgrind\n\nc"
valgrind --leak-check=yes ./env &> errfile
cat errfile > cat_err

valgrind --leak-check=full --show-leak-kinds=all -v --leak-check=yes env -i ./env  &> errfile;
cat errfile >> cat_err

valgrind --leak-check=full --show-leak-kinds=all -v --leak-check=yes env -i foo=bar ./env  &> errfile;
cat errfile >> cat_err

valgrind --leak-check=full --show-leak-kinds=all -v --leak-check=yes env -i foo=bar ./env foo2=bar2   &> errfile;
cat errfile >> cat_err

valgrind --leak-check=full --show-leak-kinds=all -v --leak-check=yes ./env -i foo=bar ./test_exec these are my args  &> errfile;
cat errfile >> cat_err

valgrind --leak-check=full --show-leak-kinds=all -v --leak-check=yes env -i foo=bar ./env infoo=inbar ./test_exec these are the args  &> errfile;
cat errfile >> cat_err

valgrind --leak-check=full --show-leak-kinds=all -v --leak-check=yes env -i ./env -i             &> errfile;
cat errfile >> cat_err

valgrind --leak-check=full --show-leak-kinds=all -v --leak-check=yes env -i foo=bar ./env -i     &> errfile;
cat errfile >> cat_err

valgrind --leak-check=full --show-leak-kinds=all -v --leak-check=yes ./env -i foo=bar blah 	2 &> errfile;
cat errfile >> cat_err


cat cat_err | grep 'ERROR\|LEAK\|lost'

rm cat_err errfile
