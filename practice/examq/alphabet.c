#include <stdlib.h> 
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <signal.h>

int main(int argc, char** argv) {

	int fd[2];

	if(pipe(fd) < 0) {
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	

	for(char c='a';c<='z';c++) {

		if(write(fd[1], &c, 1) < 0) {
			perror("putc");
			exit(EXIT_FAILURE);
		}
	}

	close(fd[1]);

	int NUM_PROCESSES = 10;

	char prog_name[] = "prog_"; 
	char* args[2];
	args[0] = prog_name;
		
	args[1] = NULL;

	pid_t pid;
	for(int i=0;i<NUM_PROCESSES;i++) {
		pid = fork();

		if(pid == 0) { /* in the child */
		

			args[0][4] = '0'+i;

			dup2(fd[0], 0); /* redirect stdin to read from pipe */

			execv(args[0], args);


			perror("exec");
			
			close(fd[0]);
			_exit(EXIT_FAILURE);
		}
	}


	for(int i=0;i<NUM_PROCESSES;i++) {
		int status = -1; 
		pid_t pid;
		if ( ( (pid = wait(&status)) < 0 ) ) {
			perror("wait");
		}

		char* exit_status = NULL;
		char* desc = NULL;
		int num_stat = -1;


		if ( WIFEXITED(status) ) {
			/* exited */
			exit_status = "exited";
			desc = "status";
			num_stat = WEXITSTATUS(status);
		} else if ( WIFSIGNALED(status) ) {
			/* signaled */
			exit_status = "killed";
			desc = "signal";
			num_stat = WTERMSIG(status);
		} else if ( WIFSTOPPED(status) ) {
			exit_status = "stopped";
			desc = "status";
			num_stat = W_STOPCODE(status);
		}


		printf("pid: %u %s with %s %d.\n", pid, exit_status, desc, num_stat);

	}

	return 0;
}