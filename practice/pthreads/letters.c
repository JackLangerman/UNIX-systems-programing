#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>


/* linked list of messages */
typedef struct message Message;
struct message {
	char c;
	Message* next;
};

Message* init_message(Message* m) {
	m->c = ':';
	m->next = NULL;
}

void print_list_helper(Message* first) {
	printf("%c", first->c);
	if(first->next) print_list_helper(first->next);
}
void print_list(Message* first) {
	print_list_helper(first);
	printf("\n");
}

void free_list(Message* first) {
	if(first->next) free_list(first->next);
	free(first);
}
/* end linked list of messages */


/* producer */
void* produce(void* args) {
	// printf("producer: ");
	Message* first = (Message*) args;
	Message* next;

	for (char letter = 'a'; letter <= 'z'; letter++) {
		next->next = malloc(sizeof(Message));
		next = next->next;
		init_message(next);
		next->c = letter;
	}
	// printf("\n");
	return NULL;
}
/* end producer */

void* consume(void* args) {
	Message* next = (Message*) args;
	while(true) {
		if(next->next == NULL) break;
		next = next->next;
		sleep(1);
		printf("%c", next->c);
		fflush(stdout);
	}
	printf("\n");
}

int main(int argc, char* argv[]) {
	Message* first = malloc(sizeof(Message));
	init_message(first);
	// first->c = 'a';
	// first->next = NULL;

	// Message* cur = malloc(sizeof(Message));
	// init_message(cur);
	// cur->c = 'b';

	// first->next = cur;

	// print_list(first);

	// free_list(first);


	produce(first);
	// print_list(first);
	consume(first);
	free_list(first);

	exit(0);
}









