#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

#define CMDLEN 4096
#define MAXARGS 32
#define WHITESPACE "\t \n\r"



bool shell();
void handle(int sig);

int main(int argc, char* argv[]) {

	struct sigaction action;
	action.sa_handler = handle;
	action.sa_flags = SA_RESTART;

	sigemptyset(&action.sa_mask);

	if(sigaction(SIGINT , &action, NULL) == -1) { perror("jsh: sigaction"); exit(EXIT_FAILURE); }
	if(sigaction(SIGQUIT, &action, NULL) == -1) { perror("jsh: sigaction"); exit(EXIT_FAILURE); }


	if ( shell() )
		return 1;
	else 
		return 0;

} /* end main */

void handle(int sig) {}

void get_input(char* input) {
	char* return_val = fgets(input, CMDLEN, stdin);
	if(return_val == NULL) {
		printf("\n\n[Process completed]\n\n");
		exit(EXIT_SUCCESS);
	}
	if (!input) {
		perror("fgets");
		exit(EXIT_FAILURE);
	}
} /* end get_input */

bool maybe_builtin(char* input, char** args, char** dollar_question) {
	if(input) {
		char* p = input;
		for ( ; *p; ++p) *p = tolower(*p);  /* convert string to lowercase */

		if(strcmp(input, "exit") == 0) {
			printf("exiting...bye...\n");
			
			free(*dollar_question);
			free(args);

			exit(EXIT_SUCCESS);
		} else if (strcmp(input, "cd") == 0) {
			if(args[1])
				if (chdir(args[1]) != 0) perror("chdir"); 
			return true;
		}
	} 
	return false;
}

char** tokenize(char* input, char** infile, char** outfile, 
				char** append_outfile, int* fd_to, char** dollar_question) {	

	char* token = strtok(input, WHITESPACE);
	/* remove io redirection */ 
	char* cmd = NULL; /* assume the first token is the command */
	/*if (!maybe_builtin(cmd)) { */  /* if cmd does not refer to a builtin */
	
	size_t cap = MAXARGS; 				/* inital capacity of argv */
	size_t argnum = 0;			 	/* index into argv */
	char** args = calloc(cap, sizeof(char*));	/* allocate argv */
	/* error check calloc */

	*fd_to = -1;

	while(token) {								/* loop through input nulling delims */
		if(strcmp(token, ">")==0) {
			*outfile = strtok(NULL, WHITESPACE); 			/* get output filename */
			*fd_to = 1;
		} else if( strcmp(token, ">>" )==0 ) {
			*append_outfile = strtok(NULL, WHITESPACE);
			*fd_to = 1;
		} else if(strcmp(token, "2>")==0) {
			*outfile = strtok(NULL, WHITESPACE); 			/* get err output filename */
			*fd_to = 2;
		} else if( strcmp(token, "2>>" )==0 ) {
			*append_outfile = strtok(NULL, WHITESPACE);
			*fd_to = 2;
		} else if( strcmp(token, "<")==0) {
			*infile = strtok(NULL, WHITESPACE);				/* get input filename */
			*fd_to = 0;
		} else {
			if(strcmp(token, "$?")==0) 
				args[argnum++] = *dollar_question;				/* expand $? */
			else
				args[argnum++] = token;							/* put token in argv  */
			
			if(cmd == NULL) cmd = token; 						/* first non-redirect is cmd */
		}

		/*printf("token: %s\n", token);*/
		token = strtok(NULL, WHITESPACE); 					/* get the next token */

	}

	if(maybe_builtin(cmd, args, dollar_question)) return NULL; 	/* return null if a builtin was executed */
	else return args;	 	 		/* return the new argv */
} /* end tokenize */


#define IN_FLAGS (O_RDONLY)
#define OUT_APP_FLAGS (O_WRONLY | O_APPEND | O_CREAT)
#define OUT_FLAGS (O_WRONLY | O_TRUNC | O_CREAT)

void redirect(int fd_from, char* path_to, int flags) {
	int out_fd = open(path_to, flags, 
						S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

	printf("for filename %s w/ descrptor %d now to %d\n", path_to, out_fd, fd_from);

	if(dup2(out_fd, fd_from)==-1) { perror("jsh: dup2"); _exit(EXIT_FAILURE); }

	close(out_fd);
}


bool shell() {
	char* prompt = "~> $";

	/* check if the user has a custom prompt and use it if they do */
	const char* usr_prompt = getenv("PS1");
	if (usr_prompt != NULL) prompt = (char*) usr_prompt; 

	char* dollar_question = malloc(4);

	/* loop until exit */
	while(true) {
		printf("%s ", prompt);
		char input[CMDLEN]; /* allocate a buffer to store command */
		get_input(input);     /* read command from stdin */

		char* infile =NULL; 
		char* outfile=NULL;
		char* append_outfile=NULL;
		int fd_num  = -1;


		char** args = tokenize(input, &infile, &outfile, &append_outfile, &fd_num, &dollar_question); 
		/*printf("new outfile is %s\n", outfile);*/
		/*printf("args is : %s||\n", args[0]); */
		if (args) { /* if args != NULL i.e. if we did not go into a builtin in tokenize */
			pid_t pid = fork();

			if (pid == -1) { perror("fork"); continue; }

			/* printf("ppid: %zu pid: %zu", ppid, pid); */
			if (pid==0) { /* in the child */
				/* printf("\nin the child, calling exec on %s...bye.\n", input); */
				/* printf("args[0] = %s before exec", args[0]); */

				/* handle io redirection */

				fflush(stdout);
				if(outfile != NULL) redirect(fd_num, outfile, OUT_FLAGS);
				else if(append_outfile != NULL) redirect(fd_num, append_outfile, OUT_APP_FLAGS);
				else /*printf("noredir\n")*/;

				if(infile != NULL) redirect(0, infile, IN_FLAGS);

				/*printf("$? = %s\n", dollar_question);*/
				fflush(stdout);

				if(args[0]) execvp(args[0], args); 
				else {
					free(args); /* free args which were allocated in tokenize */
					/*printf("exit no exec");*/
					exit(EXIT_SUCCESS);
				}

				perror("jsh: exec");
				free(args); /* free args which were allocated in tokenize */
				_exit(EXIT_FAILURE);

			} else { /* in the parent */
				/*printf("hello from the parent   ");*/
				int return_status;
				/*printf("waiting on %u", pid);*/
				fflush(stdout);
				pid_t w = waitpid(pid, &return_status, 0);
				if (w == -1) { perror("waitpid"); free(args); continue; }
				/*printf("back in the parent");*/
				fflush(stdout);

				if (WIFSIGNALED(return_status)) { 
					/*printf("stopped by signal: %d", WTERMSIG(return_status));*/
					sprintf(dollar_question, "%d", 127+WTERMSIG(return_status));
									fflush(stdout);

				} else {
					/*printf("return_status: %d\n", return_status);*/
					/*printf("\n", );*/
					fflush(stdout);


					sprintf(dollar_question, "%d", return_status);
					/*printf("%s/n", dollar_question);*/

					fflush(stdout);

				}

				/* printf("\nin the parent, continuing...\n"); *//* in the parent */ 
			} /* end if in parent */

			/* if all goes well the child will not execute anything 
 * 			   after this point because execvp will never return. */

			free(args); /* free args which were allocated in tokenize */
			/* free(dollar_question); */

		} /* end if args */

	} /* end while(true) */

	fprintf(stderr, "should not be here... exiting anyway...\n");
	return false;
} /* end shell() function */








