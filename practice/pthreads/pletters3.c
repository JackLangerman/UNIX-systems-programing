#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>


/* linked list of messages */
typedef struct message Message;
struct message {
	char c;
	Message* next;
};

void init_message(Message* m) {
	m->c = ':';
	m->next = NULL;
}

void print_list_helper(Message* first) {
	printf("%c", first->c);
	if(first->next) print_list_helper(first->next);
}
void print_list(Message* first) {
	print_list_helper(first);
	printf("\n");
}

void free_list(Message* first) {
	if(first->next) free_list(first->next);
	free(first);
}

typedef struct list List;
struct list {
	Message** first;
	Message** last;
	pthread_mutex_t* lock;
	bool done;
};

/* end linked list of messages */


/* producer */
void* produce(void* args) {
	printf("start producing\n");

	List hwl = *((List*) args); // get a copy of args as List

	
	pthread_mutex_t* lock = hwl.lock; // address of the real lock
	

	for (char letter = 'a'; letter <= 'z'; letter++) {
		

		// allocate space for new message and prepare it
		Message* new_message = malloc(sizeof(Message)); 
		if(new_message == NULL) {
			char err[] = "on _ malloc:";
			sprintf(err, "on %c malloc:", letter);
			perror(err);
		}
		
		init_message(new_message);
		new_message->c = letter;

		sleep(1); // sleep to simulate working on stuff


		pthread_mutex_lock(lock); // aquire the lock
		
		(*(hwl.last))->next = new_message;   // save the new message as next
		hwl.last = &((*(hwl.last))->next);   // bump up tail pointer to tail->next
		
		printf("produce %c\n", letter);
		fflush(stdout);

		pthread_mutex_unlock(lock); // release the lock
	
	} // done producing (end for)



	pthread_mutex_lock(lock); 		// aquire the lock

	((List*) args)->done = true;	// tell everyone producion has concluded

	pthread_mutex_unlock(lock); 	// release the lock

	printf("done producing\n");
	fflush(stdout);

	return NULL;
}
/* end producer */


/* consumer */
void* consume(void* args) {
	printf("\t\t\t\tstart consumer %lu\n", (unsigned long) pthread_self());
	fflush(stdout);
	List* hwl = (List*) args;				// get pointer to List
	pthread_mutex_t* lock = hwl->lock;		// get pointer to lock

	while(true) {
		pthread_mutex_lock(lock);			// aquire lock
		Message* head = *(hwl->first);		// get pointer to head
		
		if(head->next == NULL) {			// if there is a next item in the list
			if(hwl->done) {						// if producer says they are done
				pthread_mutex_unlock(lock); 		// relase the lock
				break;								// break out of loop and terminate
			} else {							// else if produer not done, but list empty
				pthread_mutex_unlock(lock);			// relsease the lock
				continue;							// go around again
			} //
		}// end if list is empty

		// if we are here we have something to consume

		Message* old = head;			// save a pointer to head (what we will consume)
		*(hwl->first) = head->next;		// bump up head pointer past the thing we will consume
		pthread_mutex_unlock(lock);		// release the lock

		
		sleep(10);						// sleep to simulate doing some work

		printf("\t\t\t\t%lu: %c\n", (unsigned long) pthread_self(), old->next->c); // print results
		fflush(stdout);

		free(old);						// free the thing now that we have consumed it


		// return NULL;
	} // end consumption loop

	printf("\t\t\t\tend consumer %lu\n", (unsigned long) pthread_self());
	fflush(stdout);
	return NULL;
}
/* end consumer */


int main(int argc, char* argv[]) {
	// setup / initalize the linked list with a dummy head
	Message* HEAD = malloc(sizeof(Message));
	if(HEAD == NULL) {
		perror("malloc:");
		exit(EXIT_FAILURE);
	}
	init_message(HEAD);

	pthread_mutex_t list_lock = PTHREAD_MUTEX_INITIALIZER;

	List hwl;
	hwl.first = &HEAD;
	hwl.last = &HEAD;
	hwl.lock = &list_lock;
	hwl.done = false;


	// done setting up list, start the producer thread
	
	printf("List is setup. About to produce.\n");
	pthread_t prod_t;
	pthread_create(&prod_t, NULL, produce, &hwl);


	// production thread should be running, about to start up consumers

	printf("Producer running. About to consume.\n");
	

	sleep(5);  // sleep to let producer get a few things in the queue


	//start up num_threads consumers
	int num_threads = 10;
	pthread_t consumers[num_threads];

	for (int i=0;i<num_threads;i++) {
		pthread_create(&consumers[i], NULL, consume, &hwl);
		sleep(1);
	}

	// join the consumers
	for (int i=0;i<num_threads;i++) 
		pthread_join(consumers[i], NULL);

	// join the producer 
	pthread_join(prod_t, NULL);

	// exit(0);
	return 0;
}









