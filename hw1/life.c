#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEFAULT_ROWS 10
#define DEFAULT_COLS 10
#define DEFAULT_FILE "life.txt"
#define DEFAULT_GENERATIONS 10

int parseArgs(int argc, char* argv[], int* rows, int* cols, char** filename, int* generations);
void printBoard(char **array, int rows, int cols );
char** allocateBoard(int r, int c);
void parseFile(char** board, char* filename, int rows, int cols);
void life(char** board, int rows, int cols);


int main(int argc, char* argv[]) {

	// allocate variables to store paramiters of sim
	char* filename = "before";
	int rows, cols, generations;

	// parse the command line arguments to the sim paramiter variables
	parseArgs(argc, argv, &rows, &cols, &filename, &generations);

	// allocate the board
	char** board = allocateBoard(rows, cols);

	// load the initial state of the sim from filename into board
	parseFile(board, filename, rows, cols);

	//for each generation
	for(int i=0;i <= generations;i++) { 
		// display the board
		printf("Generation %d:\n", i);
		printBoard(board, rows, cols);

		// run the sim
		life(board, rows, cols);
		printf("================================\n");
	}

	// cleanup 
	free (board);
	return 0;
}


// parse command line arguments from argc, argv 
// and put parsed values in rows, cols, filename, generations
int parseArgs(int argc, char* argv[], int* rows, int* cols, char** filename, int* generations) {

	// set default values
	*rows = DEFAULT_ROWS;
	*cols = DEFAULT_COLS;
	*filename = DEFAULT_FILE;
	*generations = DEFAULT_GENERATIONS;

	// if user has provided arguments for non-default values parse
	// them and save to the appropriate return variables
	if(argc >= 2) {
		char** end_ptr;
		*rows = strtol(argv[1], end_ptr, 10);

		if(argc >= 3) {
			*cols = strtol(argv[2], end_ptr, 10);

			if (argc >= 4){
				*filename = argv[3];

				if (argc >= 5) {
					*generations = strtol(argv[4], end_ptr, 10);

				}
			}
		}
	}

	return 0;
} // end parseArgs


void printBoard(char **array, int rows, int cols ) {
    for(int i = 1; i < rows+1; i++ ){
        for(int j = 1; j < cols+1; j++){
            printf( "%c", array[i][j] );
        }
        printf( "\n" );
    }
} // end print board


char** allocateBoard(int r, int c) {
	int rows = r+2;  // add padding of 1 cell around board to deal with edges/corners
	int cols = c+2;
	char** board;

	// allocate space on the heap
	board = (char **) malloc(rows * sizeof(char*));
	for (int row = 0; row<rows; row++) {
		board[row] = (char *) malloc(cols * sizeof(char));
	}

	// mark all cells "dead"
	// memset(board, '-', rows*cols);
	for (int row = 0; row<rows; row++) {
		for (int col = 0; col<cols; col++) {
			board[row][col] = '-'; 
		}
	}

	return board; //return a pointer to the first cell
} // end allocate board


// read in an innitial board state from a file
void parseFile(char** board, char* filename, int rows, int cols) {
	
	// open the file for reading
	FILE *input = fopen(filename, "r");
	if (input == NULL) {
		printf("Unable to open file.\n");
		exit(1);
	}


	for(int i=1;i<rows+1;i++) {
		int c = getc(input);

		if (c==EOF) break; // if the end of the file is reached, stop reading

		int j = 1;
		while(c != '\n') {
			// printf("|%c|", c);
			board[i][j] = (c=='*'?'*':'-');  // if a cell is alive, mark it alive, otherwise dead
			c = getc(input);
			j++;
		}
	}

	fclose(input); // close file
}

// the game of life
void life(char** board, int rows, int cols) {
	unsigned char neighborcounts[rows][cols];

	// count neighbors
	for (int row = 1; row<rows+1; row++) {
		for (int col = 1; col<cols+1; col++) {
			
			int count = 0;
			for (int ofsr=-1;ofsr<=1;ofsr++) {
				for (int ofsc=-1;ofsc<=1;ofsc++) {
					if (board[row+ofsr][col+ofsc] == '*' && !(ofsc==0 && ofsr==0)) count++;
				}	
			}
			neighborcounts[row-1][col-1] = count;
		}
	}


	// implement rules of life
	for (int row = 1; row<rows+1; row++) {
		for (int col = 1; col<cols+1; col++) {

			
			if (board[row][col] == '*') { // if cell is "alive"

				if (neighborcounts[row-1][col-1] < 2)
					board[row][col] = '-';

				else if (neighborcounts[row-1][col-1] < 4)
					board[row][col] = board[row][col]; // noop

				else if (neighborcounts[row-1][col-1] > 3)
					board[row][col] = '-';

			} else if (neighborcounts[row-1][col-1] ==3) { // cell is dead and has 3 neighbors
				board[row][col] = '*';
			}

		}
	}
}
